# lakefs

### Развертка lakefs

Для того чтобы развернуть контейнер с lakefs, подключенный к postgres и s3 хранилищу, нужно запустить команду

```shell
docker-compose up
```

С контейнером lakefs настроен bind mount папки app, в котором лежит 2 версии файла и Makefile.

### Работа с lakefs

Для загрузки тестового файла в lakefs сначала надо зайти в развернутый контейнер
```shell
docker exec -it -u 0 lakefs-example-lakefs-1 sh
```
затем установить make 
```shell
apk update && apk add make
```
и запустить pipeline
```shell
cd app/ &&  make
```
в нем загрузится 2 версии файла, затем с lakefs скачается файл.